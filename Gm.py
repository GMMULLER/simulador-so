# -*- coding: utf-8 -*-

from Mp import *
import Params
# from Params import *
from ProcessPCB import *
from TabelaDePagina import *
from Pagina import *
import math

class Gm:

    """ Singleton Pattern """
    __instance = None
    pos_arq = 0 #Sempre começa da posição inicial do arquivo de entradas

    @staticmethod
    def getInstance(nome_arquivo, mp):
        
        if Gm.__instance == None:
            Gm(nome_arquivo, mp)
        
        return Gm.__instance
    
    #É necessário iniciar o gerenciado de memória passando o arquivo de entrada
    def __init__(self, nome_arquivo, mp):
        
        self.log = []
                
        if Gm.__instance != None:
            raise Exception("This class is a singleton")
        else:
            Gm.__instance = self
            self.nome_arquivo = nome_arquivo
            self.mp = mp
            self.arrayLRU = []
            self.freeMsMem = Params.TAM_MS

    #Recebe endereço lógico do processador e transforma em endereço físico
    def decodeEnd(self, endLogico, processId):
        instBin = bin(endLogico)
        instBin = instBin[2:]
        tamanhoInst = len(instBin)
        if(tamanhoInst > Params.TAM_END_LOGICO): #Se o programa está requisitando um endereço maior do que o endereço lógico
            self.log.append("Endereço lógico refenciado por "+processId+" é maior que o tamanho máximo do end lógico. Abortado!\n")
            self.exitProcess(processId) #Aborta processo
            return None
        while(tamanhoInst < Params.TAM_END_LOGICO):
            instBin = "0" + instBin
            tamanhoInst += 1

        pagBin = instBin[0:Params.TAM_END_PAG] #Separando o endereço da entrada da TP
        deslocBin = instBin[Params.TAM_END_PAG:] #Separando o offset do quadro

        arrayPT = self.mp.getArrayPT() #Recupera a lista de processos submetidos
       
        if(not arrayPT): #Se a lista de processos está vazia
            raise Exception("Não há processos submetidos")
            return None

        process = None
        for p in arrayPT:
            if(p.id == processId): #Recupera o processo que o endereço lógico referencia
                process = p
                break

        if(process == None):
            raise Exception("Processo não submetido ainda")
            return None

        if(endLogico+1 > process.size): #Se o endereço lógico aponta para algo fora da memória do processo
            self.exitProcess(processId) #Aborta processo
            self.log.append("Processo "+processId+": Pagination fault. Abortado!")
            return None

        arrayTP = self.mp.getArrayTP() 
        processTP = arrayTP[process.indiceTP] #Recupera a tabela de páginas do processo
        
        processNPages = int(math.ceil(process.size/Params.TAM_QUADROS)) #Calcula quantas páginas tem o processo
        
        pagEndDec = int(pagBin, 2) #Endereco da entrada da TP em decimal

        # if(pagEndDec+1 > processNPages or pagEndDec+1 > pow(2, TAM_END_PAG)): #Se a página que está sendo endereçada está fora do escopo do processo
        #     self.exitProcess(processId)
        #     return None

        entrada = processTP.getEntrada(pagEndDec)
        quadro = None

        if(entrada[0] == None or entrada[0] == 0): #Testa se página não está carregada
            pagPos = self.mp.proxPosLivre()
            self.log.append("Processo "+processId+" page fault na página "+str(pagEndDec))
            if(pagPos == -1): #MP está toda ocupada
                loadedPage = self.readImagePage(processId, pagEndDec)
                if(Params.POLITICA_SUBS == 0):
                    quadroDec = self.arrayLRU[0]
                    quadro = bin(quadroDec)[2:]
                    self.log.append("Usando LRU e carregando página "+str(pagEndDec)+" do processo "+processId+" para o quadro "+str(quadroDec))
                    self.replaceFrame(quadroDec, loadedPage)
                else:
                    quadroDec = self.clock()
                    quadro = bin(quadroDec)[2:]
                    self.log.append("Usando Clock e carregando página "+str(pagEndDec)+" do processo "+processId+" para o quadro "+str(quadroDec))
                    self.replaceFrame(quadroDec, loadedPage)
            else:
                quadro = bin(pagPos)[2:]
                pagina = self.readImagePage(processId, pagEndDec)
                tamanhoQuadro = len(quadro)
                while(tamanhoQuadro < Params.TAM_END_QUADROS): #Tranforma o endereço do quadro em decimal válido
                    quadro = "0" + quadro
                    tamanhoQuadro += 1
                processTP.modEntrada(pagEndDec, 1, 0, quadro)
                self.log.append("Alocando quadro "+str(pagPos))
                self.mp.setQuadro(pagina, pagPos, 1)
                self.attLRU(pagPos)
        else: #Caso esteja
            quadro = entrada[2] #Recupera quadro em binario
            self.mp.modU(int(quadro, 2), 1) #Atualiza o bit de utilização para 1
            self.attLRU(int(quadro, 2))

        tamanhoQuadro = len(quadro)
        while(tamanhoQuadro < Params.TAM_END_QUADROS): #Ajeita o tamanho do endereço binário do quadro
            quadro = "0" + quadro
            tamanhoQuadro += 1

        fisicBin = quadro + deslocBin #Concatena quadro mais deslocamento em binário

        return fisicBin

    #Lê instrução do arquivo
    def le_instrucao(self):
        arq = open(self.nome_arquivo, 'r') #abre arquivo
        
        arq.seek(0, 0)
        
        conteudo = ""
        
        for i in range(0, self.pos_arq+1): #Lê linha por linha
            conteudo = arq.readline() #grava string da instrução
            if(conteudo == ""):
                return None

        self.pos_arq += 1 #grava posição da próxima instrução
        arq.close() #fecha arquivo
        
        return conteudo

    #Encapsula todo o processo de leitura e interpretação de entradas
    def buscaProxEntrada(self):
        instrucao = self.le_instrucao()
        if(instrucao == None):
            return -1
        self.decodeInst(instrucao)

    #Interpreta a instrução lida do arquivo
    def decodeInst(self, instrucao):
        splitInst = instrucao.split(" ")

        process = None
        for p in self.mp.arrayPT:
            if(p.id == splitInst[0]):
                process = p
                if(p.state == "exited"): #Pula todas as instruções de quem está abortado
                    return 1
                elif(p.state == "finished"):
                    raise Exception("Processo "+p.id+" já foi finalizado")
                    return

        #Verifica qual comando é
        if(splitInst[1] == "P"):
            if(process != None): #Se processo já está criado
                if(process.state == "blocked" or process.state == "blocked-suspended"):
                    raise Exception("Processo "+process.id+" não foi desbloqueado")
                    return
            self.instP(int(splitInst[2]), splitInst[0])
        elif(splitInst[1] == "I"):
            if(process != None): #Se processo já está criado
                if(process.state == "blocked" or process.state == "blocked-suspended"):
                    raise Exception("Processo "+process.id+" já está esperando I/O")
                    return
            self.instI(int(splitInst[2]), splitInst[0])
        elif(splitInst[1] == "R"):
            if(process != None): #Se processo já está criado
                if(process.state == "blocked" or process.state == "blocked-suspended"):
                    raise Exception("Processo "+process.id+" não foi desbloqueado")
                    return
            self.instR(int(splitInst[2]), splitInst[0])
        elif(splitInst[1] == "W"):
            if(process != None): #Se processo já está criado
                if(process.state == "blocked" or process.state == "blocked-suspended"):
                    raise Exception("Processo "+process.id+" não foi desbloqueado")
                    return
            self.instW(int(splitInst[2]), int(splitInst[3]), splitInst[0])
        elif(splitInst[1] == "C"):
            if(process != None): #Se processo já está criado
                raise Exception("Processo "+process.id+" já foi criado")
            self.instC(int(splitInst[2]), splitInst[3], splitInst[0])
        elif(splitInst[1].replace("\n", "") == "D"):
            if(process != None): #Se processo já está criado
                if(process.state != "blocked" and process.state != "blocked-suspended"):
                    raise Exception("Processo "+process.id+" não está bloqueado")
                    return
            self.instD(splitInst[0])
        elif(splitInst[1].replace("\n", "") == "T"):
            if(process != None): #Se processo já está criado
                if(process.state != "running" and process.state != "ready"):
                    raise Exception("Processo "+process.id+" não pode ser finalizado no estado blocked ou suspended")
                    return
            self.instT(splitInst[0])
        # elif(splitInst[1].replace("\n", "") == "S"):
        #     self.instS(splitInst[0])
        # elif(splitInst[1] == "LS"):
        #     self.instLS(splitInst[0], int(splitInst[2]))


    #Busca intrução para execução (soma ou subtração)
    def instP(self, endereco, processId):
        enderecoFisico = self.decodeEnd(endereco, processId) #Calcula endereço físico e carrega a página se necessário
        if(enderecoFisico == None): #Se não foi possível calcular o endereço físico
            return
        quadro = enderecoFisico[0:Params.TAM_END_QUADROS]
        quadro = int(quadro, 2)
        self.setRunning(processId)
        self.log.append("Processo "+processId+" está executando instrução de endereço lógico: "+str(endereco)+" | fisico (quadro): "+str(quadro)+" | físico (linha): "+str(int(enderecoFisico, 2)))

    #Pedido de I/O em um dispositivo
    def instI(self, dispositivo, processId):
        self.setRunning(processId) #Se ele requisitou I/O estava executando algo
        for p in self.mp.arrayPT:
            if(p.id == processId):
                self.log.append("Processo "+p.id+" requisitou I/O e foi bloqueado")
                p.state = "blocked"

    #Lê do endereço lógico de memória
    def instR(self, endereco, processId):
        enderecoFisico = self.decodeEnd(endereco, processId)
        if(enderecoFisico == None): #Se não foi possível calcular o endereço físico
            return
        quadro = enderecoFisico[0:Params.TAM_END_QUADROS]
        quadro = int(quadro, 2)
        pagina = self.mp.array[quadro][0]
        self.setRunning(processId)
        self.log.append("Processo "+processId+" leu endereço lógico: "+str(endereco)+" | fisico (quadro): "+str(quadro)+" | físico (linha): "+str(int(enderecoFisico, 2)))

    #Grava valor no endereço lógico de memória
    def instW(self, endereco, valor, processId):
        end_fisico = self.decodeEnd(endereco, processId)
        if(end_fisico == None): #Se não foi possível calcular o endereço físico
            return
        quadro = end_fisico[0:Params.TAM_END_QUADROS]      
        quadro = int(quadro, 2)
        pagina = self.mp.getQuadro(quadro)[0]
        pagina.conteudo = valor
        self.mp.setQuadro(pagina, quadro, 1)
        for p in self.mp.arrayPT:
            if(p.id == processId):
                tp = self.mp.arrayTP[p.indiceTP]
                entrada = tp.getEntrada(pagina.num_pag)
                tp.modEntrada(pagina.num_pag, 1, 1, entrada[2]) #Seta bit de modificacão
        self.setRunning(processId)
        self.log.append("Processo "+processId+" escreveu: "+str(valor)+" no endereço lógico: "+str(endereco)+" | fisico (quadro): "+str(quadro)+" | físico (linha): "+str(int(end_fisico, 2)))

    #Cria imagem do processo dado tamanho (O log pode mandar criar um processo quando mp estiver cheia?)
    def instC(self, tamanho, tipoTamanho, processId):
    
        f = open(processId+".txt", "w+") #Cria a imagem do processo em disco como um arquivo txt
        tam = None
        tipoTamanho = tipoTamanho.replace("\n", "")
                
        #Pensar nisso melhor
        if(tipoTamanho == "GB"):
            tam = tamanho * pow(2, 30) #Em bytes
        elif(tipoTamanho == "MB"):
            tam = tamanho * pow(2, 20) #Em bytes
        elif(tipoTamanho == "KB"):
            tam = tamanho * pow(2, 10) #Em bytes

        qtd_pag = int(tam/Params.TAM_QUADROS) #Quantidade de páginas no processo
        n_pag_max = pow(2, TAM_END_PAG) #Número máximo de páginas em um processo

        if(qtd_pag > n_pag_max):
            self.log.append("Processo "+processId+" não pode ser criado pois não é completamente endereçável via tp")
            process = ProcessPCB("exited", processId, tam, -1) #É criado como exited e tem todas as instruções puladas
            self.mp.addProcess(process)        
            return

        if(tam > self.freeMsMem):
            self.log.append("Processo "+processId+" não pode ser criado pois não tem espaço em MS")
            process = ProcessPCB("exited", processId, tam, -1) #É criado como exited e tem todas as instruções puladas
            self.mp.addProcess(process)            
            return

        tabelaPag = TabelaDePagina()
        indiceTP = self.mp.addTabPag(tabelaPag)

        self.freeMsMem -= tam #Deduz a quantidade livre de memória secundária
        process = ProcessPCB("ready", processId, tam, indiceTP) #Teria um estado new?
        self.mp.addProcess(process)

        for i in range(0, int(tam/Params.TAM_QUADROS)): #Criando páginas em arquivo
            f.write(str(i)+"\n")

        f.seek(0, 0)

        #Carrega duas páginas do processo para memória principal
        pagPos1 = self.mp.proxPosLivre()
        pag1 = Pagina(int(f.readline().replace("\n", "")), 0, processId)        
        if(pagPos1 == -1): #Se não há espaço retira as páginas de outros processos
            if(Params.POLITICA_SUBS == 0):
                pagPos1 = self.arrayLRU[0]
                self.replaceFrame(pagPos1, pag1)
            else:
                pagPos1 = self.clock()
                self.replaceFrame(pagPos1, pag1)
        else:
            self.mp.setQuadro(pag1, pagPos1, 1) #Insere conteúdo da primeira página
            self.attLRU(pagPos1)
        
        pagPos2 = self.mp.proxPosLivre()
        pag2 = Pagina(int(f.readline().replace("\n", "")), 1, processId)
        if(pagPos2 == -1): #Se não há espaço retira as páginas de outros processos
            if(Params.POLITICA_SUBS == 0):
                pagPos2 = self.arrayLRU[0]
                self.replaceFrame(pagPos2, pag2)
            else:
                pagPos2 = self.clock()
                self.replaceFrame(pagPos2, pag2)
        else:
            self.mp.setQuadro(pag2, pagPos2, 1) #Insere conteúdo da segunda página
            self.attLRU(pagPos2)

        pagPos1Bin = bin(pagPos1) #Posição da página na mp
        pagPos1Bin = pagPos1Bin[2:] #Retira 0b
        pagPos2Bin = bin(pagPos2) #Posição da página na mp
        pagPos2Bin = pagPos2Bin[2:] #Retira 0b

        #Ajeitando a número de bits do endereço binário do quadro
        tamanhoPos1Bin = len(pagPos1Bin)
        tamanhoPos2Bin = len(pagPos2Bin)
        
        while(tamanhoPos1Bin < Params.TAM_END_QUADROS):
            pagPos1Bin = "0" + pagPos1Bin
            tamanhoPos1Bin += 1
        while(tamanhoPos2Bin < Params.TAM_END_QUADROS):
            pagPos2Bin = "0" + pagPos2Bin
            tamanhoPos2Bin += 1

        tabelaPag.modEntrada(0, 1, 0, pagPos1Bin) #Modifica entrada da tabela de páginas do processo
        tabelaPag.modEntrada(1, 1, 0, pagPos2Bin) #Modifica entrada da tabela de páginas do processo
        self.log.append("Processo "+processId+" foi criado e carregado em memória")

    #Desbloqueia processo
    def instD(self, processId):
        for p in self.mp.arrayPT:
            if(p.id == processId):
                self.log.append("Processo "+p.id+" foi desbloqueado")
                if(p.state == "blocked-suspended"):
                    p.state = "ready-suspended"
                elif(p.state == "blocked"):
                    p.state = "ready"

    #Suspende um processo
    def instS(self, processId):
        for p in self.mp.arrayPT:
            if(p.id == processId):
                if(p.state == "ready"):
                    p.state = "ready-suspended"
                elif(p.state == "blocked"):
                    p.state = "blocked-suspended"
                self.log.append("Processo: "+p.id+" suspenso")
        
        self.swapOutProcess(processId)

    #Carrega processo suspenso. Passando o endereco lógico (decimal) da página que deve ser carregada (pois não temos PC)
    def instLS(self, processId, endereco):
        enderecoFisico = self.decodeEnd(endereco, processId) #Já carrega as páginas necessárias
        if(enderecoFisico == None):
            return
        for p in self.mp.arrayPT:
            if(p.id == processId):
                if(p.state == "ready-suspended"):
                    p.state = "ready"
                else:
                    p.state = "blocked"
                self.log.append("Processo: "+processId+" carregado para a memória")

    #Retorna o endereço do quadro que deve ser substituido
    def clock(self):
        while(1): #procurando um bit u = 0
            ponteiro_quadro = self.mp.prox_quadro #ponteiro aonde deve-se começar a procurar um u=0
            #print(Params.NUM_QUADROS)
            if(ponteiro_quadro == Params.NUM_QUADROS-1): #depois de carregar uma página deslocar o ponteiro para a próxima posição 
                self.mp.prox_quadro = 0
            else:
                self.mp.prox_quadro += 1

            if(self.mp.getQuadro(ponteiro_quadro)[1] == 0): #Se u=0, achou posição novo do processo
                return ponteiro_quadro                                    
                        
            self.mp.modU(ponteiro_quadro, 0) #Se bit for igual a 1, resetar valor de u
        
    #Atualiza o array de trace de uso de quadros (quadroUsado em decimal)
    def attLRU(self, quadroUsado):
        if(quadroUsado in self.arrayLRU):
            self.arrayLRU.remove(quadroUsado) #Remove da posição atual
        self.arrayLRU.append(quadroUsado) #Coloca na última posição (Elemento mais recente usado)

    #Esvazia um quadro
    def swapOutFrame(self, indexQuadro):
        self.saveFrame(indexQuadro)

        self.mp.array[indexQuadro] = None

    #Tira todas as páginas do processo de memória
    def swapOutProcess(self, processId):
        for i in range(0, len(self.mp.array)): #Retirando todas as páginas do processo
            quadro = self.mp.getQuadro(i)
            if(quadro != None):
                pagina = quadro[0]
                if(pagina.process_id == processId): #Se for uma página do processo
                    self.swapOutFrame(i)

    #Verifica se o quadro foi modificado e atualiza o valor na imagem do processo
    def saveFrame(self, indexQuadro):
        oldPage = self.mp.getQuadro(indexQuadro)[0]
        
        nOldPage = oldPage.num_pag
        processId = oldPage.process_id
        entrada = None
        bit_mod = 0
        process = None
        tp = None

        for p in self.mp.arrayPT:
            if(p.id == processId):
                tp = self.mp.arrayTP[p.indiceTP]
                entrada = tp.getEntrada(nOldPage)
                bit_mod = entrada[1] #Guardando qual foi o valor do bit de modificação
                tp.modEntrada(nOldPage, 0, 0, entrada[2])     
                process = p

        isAllPagesOff = 1 #0 -> Não, 1 -> Sim
        for ent in tp.arrayEntradas:
            if(ent[0] == 1):
                isAllPagesOff = 0 #Caso ache uma página que está em MP

        if(isAllPagesOff == 1): #Se todas as páginas do processo forem tiradas de memória suspende
            if(process.state == "blocked"):
                process.state = "blocked-suspended"
            elif(process.state == "ready"):
                process.state = "ready-suspended"

        if(bit_mod == 1): #Se ele foi modificado enquanto estava em memória
            self.log.append("Página "+str(oldPage.num_pag)+" do processo "+oldPage.process_id+" saindo de memória, atualizando os valores da imagem")
            
            with open(oldPage.process_id+".txt", "r") as file:
                data = file.readlines()

            data[oldPage.num_pag] = str(oldPage.conteudo)+'\n'

            with open(oldPage.process_id+".txt", "w") as file:
                file.writelines(data)            
        else:
            self.log.append("Página "+str(oldPage.num_pag)+" do processo "+oldPage.process_id+" saindo de memória, sem necessidade de atualizar imagem")

    #Subtitui o quadro da MP 
    def replaceFrame(self, indexQuadro, newPage):
        self.saveFrame(indexQuadro)

        quadro = bin(indexQuadro) #Último quadro usado 
        quadro = quadro[2:]
        
        tamanhoQuadro = len(quadro)
        while(tamanhoQuadro < Params.TAM_END_QUADROS): #Tranforma o endereço do quadro em decimal válido
            quadro = "0" + quadro
            tamanhoQuadro += 1

        processId = newPage.process_id
        pagEndDec = newPage.num_pag
        processTP = None

        for p in self.mp.getArrayPT():
            if(p.id == processId):
                processTP = self.mp.getArrayTP()[p.indiceTP] #Recupera a TP do processo

        processTP.modEntrada(pagEndDec, 1, 0, quadro) #Atualiza a entrada da TP
 
        if(Params.POLITICA_SUBS == 0):
            self.attLRU(indexQuadro) #Atualiza LRU

        self.mp.setQuadro(newPage, indexQuadro, 1) #Substitui o quadro com a nova página

    #Lê uma página específica de um processo. EndPage em decimal
    def readImagePage(self, processId, endPage):
        file = open(processId+".txt", "r+")

        conteudo = None
        for i in range(0, endPage+1): #Percorrendo o arquivo até a página solicitada
            conteudo = file.readline().replace("\n", "")
        
        resp = Pagina(conteudo, endPage, processId)

        return resp

    #Muda o estado do processo para running e dos demais de running para ready (perderam processador)
    def setRunning(self, processId):
        for p in self.mp.arrayPT:
            if(p.id == processId and p.state != "running"): #Se ele ainda não está executando
                if(p.state == "ready-suspended"):
                    self.log.append("Processo "+p.id+" passou do estado ready-suspended para ready e ganhou processador!")
                else:
                    self.log.append("Processo "+p.id+" ganhou processador!")
                p.state = "running"
            elif(p.id != processId and p.state == "running"): #Perde processador
                p.state = "ready"
                self.log.append("Processo "+p.id+" perdeu processador!")
    
    #Instrução T - muda status do processo para terminado e tira processo da mp
    def instT(self, processId):
        process = None
        for p in self.mp.getArrayPT():
            if(p.id == processId):
                p.state = "finished"
                process = p
                break

        self.freeMsMem += process.size #Incremente quantidade que foi liberada de memória secundária
        self.swapOutProcess(processId) #Tira as páginas do processo
        self.mp.arrayTP[process.indiceTP] = None #Exclui a tabela de páginas do processo
        self.log.append("Processo "+processId+" terminado!")

    #Aborta processo
    def exitProcess(self, processId):
        process = None
        for p in self.mp.getArrayPT():
            if(p.id == processId):
                p.state = "exited"
                process = p
                break

        self.freeMsMem += process.size #Incremente quantidade que foi liberada de memória secundária        
        self.swapOutProcess(processId) #Tira as páginas do processo
        self.mp.arrayTP[process.indiceTP] = None #Exclui a tabela de páginas do processo
        self.log.append("Processo "+processId+" foi abortado!")

    #É chamado em constantemente para checar se algum processo precisa ser suspenso
    def swapper(self):
        isAllPBlocked = 1 #1 -> Sim, 0 -> Não. Todos os processos estão bloqueados?
        for p in self.mp.getArrayPT(): #Verificando se todos os processos estão bloqueados
            if(p.state == "ready" or p.state == "running"):
                isAllPBlocked = 0 #Tem algum processo para executar
                break

        if(isAllPBlocked == 1): #Testa se mp está cheia
            for quadro in self.mp.array:
                if(quadro == None):
                    isAllPBlocked = 0
                    break

        menorTam = None
        if(isAllPBlocked == 1): #Não tem processo para executar
            for p in self.mp.getArrayPT(): #Procura um processo de menor tamanho para suspender
                if(p.state == "blocked"):
                    if(menorTam == None):
                        menorTam = p
                    elif(menorTam.size > p.size):
                        menorTam = p
            if(menorTam != None):
                self.log.append("Swapper escolheu "+menorTam.id+" para ser suspenso")
                self.instS(menorTam.id) #Suspende o processo
