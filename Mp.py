# -*- coding: utf-8 -*-

# from Params import *
import Params

class Mp:

    """ Singleton Pattern """
    __instance = None
    arrayTP = [] #Array de tabelas de páginas
    arrayPT = [] #Array de processos
    @staticmethod
    def getInstance():
        
        if Mp.__instance == None:
            Mp()
        
        return Mp.__instance
    
    def __init__(self):
        
        if Mp.__instance != None:
            raise Exception("This class is a singleton")
        else:
            Mp.__instance = self
            self.array = [None] * Params.NUM_QUADROS
            #prox_quadro é um atributo utilizado pelo clock
            self.prox_quadro = 0

    #Posição em decimal como índice do array
    def setQuadro(self, pagina, posicao, u = None):
        self.array[posicao] = [pagina, u]

    #Posição em decimal. Modifica bit de utilização
    def modU(self, posicao, u):
        aux = self.getQuadro(posicao)
        self.setQuadro(aux[0], posicao, u)

    def getQuadro(self, posicao):
        return self.array[posicao]

    #Retorna -1 se não há posição livre (retorno em decimal)
    def proxPosLivre(self):
        for i in range(0,len(self.array)):
            if(self.array[i] == None):
                return i 

        return -1

    def addTabPag(self, pagina):
        for i in range(0, len(self.arrayTP)):
            if(self.arrayTP[i] == None):
                self.arrayTP[i] = pagina
                return i
        self.arrayTP.append(pagina)
        return len(self.arrayTP) - 1

    def addProcess(self, process):
        self.arrayPT.append(process)
    
    def getArrayTP(self):
        return self.arrayTP
    
    def getArrayPT(self):
        return self.arrayPT