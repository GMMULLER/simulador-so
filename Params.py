# -*- coding: utf-8 -*-
import math

#Como alocar um processo que tem uma tabela de páginas maior do que o END logico?
TAM_END_LOGICO = 15 #Tamanho do endereço lógico em bits
NUM_QUADROS = 4 #Quantidade de quadros em mp
TAM_MS = 30000000 #Tamanho da memória secundária em bytes
TAM_QUADROS = 2048 #Tamanho dos quadros de mp em bytes
POLITICA_SUBS = 1 #0 -> LRU, 1 -> Relógio

TAM_END_QUADROS = int(math.ceil(math.log(NUM_QUADROS, 2))) #Número de bits necessários para endereçar todos os quadros
TAM_END_OFFSET = int(math.ceil(math.log(TAM_QUADROS, 2))) #Quantidade de bits para o offset dentro do quadro
TAM_END_PAG = TAM_END_LOGICO - TAM_END_OFFSET #Quantidade de bits para endereçar tabela de páginas