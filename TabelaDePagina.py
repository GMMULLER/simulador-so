# -*- coding: utf-8 -*-

from Params import *

class TabelaDePagina:

    def __init__(self):
        self.arrayEntradas = [None] * pow(2, TAM_END_PAG) #Formato do array: [[P, M, Quadro], ...]
        for i in range(0, len(self.arrayEntradas)):
            self.arrayEntradas[i] = [None, None, None]

    #Modifica entrada a partir do endereço em decimal e quadro em binário
    def modEntrada(self, end_entrada, bit_presenca, bit_mod, quadro):
        self.arrayEntradas[end_entrada][0] = bit_presenca
        self.arrayEntradas[end_entrada][1] = bit_mod
        self.arrayEntradas[end_entrada][2] = quadro
        
    #Recebe o indice do array em decimal
    def getEntrada(self, end_entrada):
        return self.arrayEntradas[end_entrada]