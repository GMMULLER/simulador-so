from tkinter import *
from random import randint
from Gm import *
from Mp import *
import Params
from ProcessPCB import *
from TabelaDePagina import *
from Pagina import *
import math
from tkinter import ttk  


class Kanvas:
    
    
    def createTabs(self, raiz): 
        #Definindo as tabs

        self.notebook = ttk.Notebook(raiz) 
        #Criando frame que mostrará as informações mudando em tempo real
        self.show = Frame(self.notebook)
        #Criando frame de configurações
        self.config = Frame(self.notebook)
    
    def instanciatingTab(self):
        self.notebook.add(self.config, text="Configurações do simulador")
        self.notebook.add(self.show, text = "Sistema em tempo real")
        self.notebook.pack(expand=1, fill='both', side="left")
        self.notebook.select(self.config)
        self.notebook.enable_traversal()
        
    def createConfigElements(self, raiz):
        
        #botão de iniciar/sair
    
        self.button = Button(self.config, text="Iníciar", command=self.callback)
        self.button.pack()
        self.button.place(x=15, y = 240)
        
        #entradas de texto
        
        self.tam_quadro = Entry(self.config)
        # self.tam_quadro.insert(0, int(pow(2, Params.TAM_END_OFFSET)))
        self.tam_quadro.insert(0, Params.TAM_QUADROS)
        self.num_quadro = Entry(self.config)
        self.num_quadro.insert(0, Params.NUM_QUADROS)
        self.tam_ms = Entry(self.config)
        self.tam_ms.insert(0, Params.TAM_MS)
        self.tam_end = Entry(self.config)
        self.tam_end.insert(0, Params.TAM_END_LOGICO)
        self.tam_mp = Entry(self.config)
        self.tam_mp.insert(0, float(self.tam_quadro.get()) * float(self.num_quadro.get()))
        self.tam_mp.config(state=DISABLED)
        self.subs_pol = Entry(self.config)
        self.subs_pol.insert(0, Params.POLITICA_SUBS)

        #label dos elementos
        self.label_tam_quadro = Label(self.config, text="Digite o tamanho do quadro")
        self.label_num_quadro = Label(self.config, text="Digite o número de quadros")
        self.label_tam_ms = Label(self.config, text="Digite o tamanho da memória secundaria")        
        self.label_tam_end = Label(self.config, text="Digite o tamanho do endereço lógico")
        self.label_tam_mp = Label(self.config, text="Tamanho da memória principal")      
        self.label_subs_pol = Label(self.config, text="Escolha a política de substituição (0 -> LRU e 1 -> Clock)")

        #titulo de config
        self.titulo_config = Label(self.config, text="Configurações do Sistema", font=("Helvetica", 20))
        #pack desses elementos
        self.tam_quadro.pack(side="left")
        self.num_quadro.pack(side="left")
        self.tam_ms.pack(side = "left")
        self.tam_end.pack(side="left")
        
        #localização desses elementos
        self.titulo_config.place(x=15, y=10)
        self.tam_quadro.place(x=210, y=60)
        self.tam_mp.place(x=230, y=120)
        self.num_quadro.place(x=210, y=90)
        self.tam_ms.place(x=300, y=150)
        self.tam_end.place(x=260, y=180)
        self.subs_pol.place(x=400, y=210)

        #localizando as labels
        self.label_tam_quadro.place(x=15, y=60)
        self.label_num_quadro.place(x=15, y=90)
        self.label_tam_mp.place(x=15, y=120)
        self.label_tam_ms.place(x=15, y=150)
        self.label_tam_end.place(x=15, y=180)
        self.label_subs_pol.place(x=15, y=210)

    def exit(self):
        exit()
        
    def callback(self):
        if (self.started == 0):
            self.notebook.select(self.show)
            self.started = 1
            self.button.config(text="Sair")
            self.set_values()
            self.mp = Mp()
            self.gm = Gm("entrada.txt", self.mp)
        else:
            exit()
            
    def set_values(self):
        Params.TAM_QUADROS = int(self.tam_quadro.get())
        Params.NUM_QUADROS = int(self.num_quadro.get())
        Params.TAM_MS = int(self.tam_ms.get())
        Params.TAM_END_QUADROS = int(math.ceil(math.log(Params.NUM_QUADROS, 2))) #Número de bits necessários para endereçar todos os quadros
        Params.TAM_END_OFFSET = int(math.ceil(math.log(Params.TAM_QUADROS, 2))) #Quantidade de bits para o offset dentro do quadro
        Params.TAM_END_PAG = Params.TAM_END_LOGICO - Params.TAM_END_OFFSET #Quantidade de bits para endereçar tabela de páginas
        Params.POLITICA_SUBS = int(self.subs_pol.get())

        self.tam_mp.config(state=NORMAL)
        self.tam_mp.delete(0, END)
        self.tam_mp.insert(0, int(self.tam_quadro.get()) * int(self.num_quadro.get()))
        self.tam_mp.config(state=DISABLED)
        Params.TAM_END_LOGICO = int(self.tam_end.get())

    def are_processes_done(self):
        if buscaProxEntrada() == -1:
            return True
        return False

    def __init__(self, raiz):

        #self.TIMER = 1000
        self.TIMER = 2000
        self.started = 0
        self.stop_loop = 0
        
        #criando tabs
        self.createTabs(raiz)
        
        #Definindo arrays onde estarão as frases que devem ser mostradas na tela
        self.array_processos = []
        self.array_log = []
        self.array_mp = []
        self.array_tp = []
        self.array_lru = []

        #Definindo arrays onde serão armazenados os componentes
        self.componentes_processos = []
        self.componentes_log = []
        self.componentes_mp = []
        self.componentes_tp = []
        self.componentes_lru = []

        #Pai dos Canvas da esquerda
        self.canvas1 = Canvas(self.show, width=330, height=640, bd=5, bg='dodgerblue')
        self.canvas1.pack(side=LEFT)

        #Canvas de Processos
        self.canvas11 = Canvas(self.canvas1, width=330, height=320, bd=5, bg='white')
        self.canvas11.pack(side=TOP)
        self.canvas11.create_text(165, 15, text="Processos", anchor=CENTER, width=330, justify=CENTER, font="Arial")

        #Canvas de Logs
        self.canvas12 = Canvas(self.canvas1, width=330, height=320, bd=5, bg='white')
        self.canvas12.pack(side=BOTTOM)
        self.canvas12.create_text(165, 15, text="Logs", anchor=CENTER, width=330, justify=CENTER, font="Arial")
        
        #Canvas da Memoria Principal
        self.canvas2 = Canvas(self.show, width=330, height=640, bd=5, bg='white')
        self.canvas2.pack(side=LEFT)
        self.canvas2.create_text(165, 15, text="Memória Principal", anchor=CENTER, width=330, justify=CENTER, font="Arial")

        #Canvas da Tabela de Paginas
        self.canvas3 = Canvas(self.show, width=330, height=640, bd=5, bg='white')
        self.canvas3.pack(side=LEFT)
        self.canvas3.create_text(165, 15, text="Tabela de Páginas", anchor=CENTER, width=330, justify=CENTER, font="Arial")

        #Canvas da LRU
        self.canvas4 = Canvas(self.show, width=330, height=640, bd=5, bg='white')
        self.canvas4.pack(side=LEFT)
        self.canvas4.create_text(165, 15, text="LRU", anchor=CENTER, width=330, justify=CENTER, font="Arial")

        #adicionando elementos de configuração
        
        self.createConfigElements(raiz)
        
        #adicionando a tab
        self.instanciatingTab()

        #Chamando pela primeira vez o loop da aplicação
        instancia.after(1000, self.loop)

    def loop(self):

        if(self.started == 1 and self.stop_loop == 0):  
                
            for k in self.componentes_processos:
                self.canvas11.delete(k)

            for k in self.componentes_log:
                self.canvas12.delete(k)

            for k in self.componentes_mp:
                self.canvas2.delete(k)

            for k in self.componentes_tp:
                self.canvas3.delete(k)
            
            for k in self.componentes_lru:
                self.canvas4.delete(k)

            #Arrays onde ficam os componentes de Texto apresentados na tela
            self.componentes_processos = []
            self.componentes_log = []
            self.componentes_mp = []
            self.componentes_tp = []
            self.componentes_lru = []

            #Arrays onde ficam as strings que devem ser impressos na tela
            self.array_processos = []
            self.array_log = []
            self.array_mp = []
            self.array_tp = []
            self.array_lru = []

            result = self.gm.buscaProxEntrada()
            self.gm.swapper()
            if(result == -1):
                self.array_log.append("Done!")
                self.stop_loop = 1
            count = 0
            for i in self.mp.array:
                if(i == None):
                    self.array_mp.append(str(count)+" | None | None | N da página no processo: None | None")
                else:
                    pagina = i[0]
                    u = i[1]
                    if(self.mp.prox_quadro == count):
                        self.array_mp.append(str(count)+" | " + pagina.process_id + " | " + str(pagina.conteudo) + " | N da página no processo: " + str(pagina.num_pag) + " | " + str(u) + " <-")   
                    else:
                        self.array_mp.append(str(count)+" | " + pagina.process_id + " | " + str(pagina.conteudo) + " | N da página no processo: " + str(pagina.num_pag) + " | " + str(u))   
                count += 1
            for i in self.mp.arrayPT:
                self.array_processos.append(i.id + " | " + i.state)
                if(i.state == "running"):
                    tp = self.mp.arrayTP[i.indiceTP]
                    if(tp != None):
                        for j in tp.arrayEntradas:
                            self.array_tp.append("P: "+str(j[0])+" | M: "+str(j[1])+" | Quadro: "+str(j[2]))

            i=60
            for texto in self.array_processos:
                self.componentes_processos.append(self.canvas11.create_text(40, i, text=texto, anchor=SW, width=270, justify=LEFT)) #anchor=CENTER serve para ser posicionado com base no centro da tela
                i+=20

            i=60
            
            for log in self.gm.log:
                self.array_log.append(log)
                
            self.text_log = ""
            for texto in self.array_log:
                self.text_log += (texto + "\n")
                
            self.componentes_log.append(self.canvas12.create_text(40, 40, text=self.text_log, anchor=NW,  width = 270, justify=LEFT))

            self.gm.log = []
            
            self.text_mp = ""
            for texto in self.array_mp:
                self.text_mp += (texto + "\n")
            self.componentes_mp.append(self.canvas2.create_text(10, 40, text=self.text_mp,anchor=NW, width=325, justify=LEFT)) #anchor=CENTER serve para ser posicionado com base no centro da tela

            self.text_tp = ""
            for texto in self.array_tp:
                self.text_tp += (texto + "\n")
            self.componentes_tp.append(self.canvas3.create_text(40, 40, text=self.text_tp, anchor=NW, width=270, justify=LEFT)) #anchor=CENTER serve para ser posicionado com base no centro da tela
            
            if(Params.POLITICA_SUBS == 0):
                self.text_lru = ""
                self.array_lru = self.gm.arrayLRU
            
                for texto in self.array_lru:
                    self.text_lru += (str(texto) + "\n")
                self.componentes_lru.append(self.canvas4.create_text(40, 40, text=self.text_lru, anchor=NW, width=270, justify=LEFT)) #anchor=CENTER serve para ser posicionado com base no centro da tela

            #A cada intervalo de tempo self.TIMER, chama a função loop para atualizar tudo.
        instancia.after(self.TIMER, self.loop)

#Definindo uma instancia da Classe Tk
instancia = Tk()

#Chamando a nossa classe com o parametro da Instancia da Classe tk
Kanvas(instancia)
instancia.geometry("1380x650")
instancia.mainloop()