#!/usr/bin/python
# -*- coding: utf-8 -*-
#Código acima serve para manter a codificação

import time

from Gm import *

TIMER = 2 #tempo entre instrucoes em segundos 

time_inicio = time.time()
mp = Mp()
gm = Gm("entrada.txt", mp)

while(True):
    #Todo o código que deve se manter acontecendo, deve estar contido dentro desse while
    # while(time.time() < time_inicio + TIMER):
        #Atualiza a interface
    
    #Essa instrução faz esperar mais 'TIMER' segundos para executar a próxima instrução,
    #portanto, deve-se fazer a instrução ser lida aqui 
    print("******************************************************")
    result = gm.buscaProxEntrada()
    gm.swapper() #Verifica a necessidade de suspender algum processo
    if(result == -1):
        print("Done!")
        break
    print("Quadros de MP:")
    count = 0
    for i in mp.array:
        if(i == None):
            print(str(count)+" | None | None | N da página no processo: None | None")
        else:
            pagina = i[0]
            u = i[1]
            print(str(count)+" | "+pagina.process_id+" | "+str(pagina.conteudo)+" | N da página no processo: "+str(pagina.num_pag)+" | "+str(u))
        count += 1
    for i in mp.arrayPT:
        print("Nome do processo: "+i.id)
        print("Estado: "+i.state)
        # tp = mp.arrayTP[i.indiceTP]
        # print("Tabela de Páginas: ")
        # for j in tp.arrayEntradas:
        #     print("P: "+str(j[0])+" | M: "+str(j[1])+" | Quadro: "+str(j[2]))
    time.sleep(2)
    time_inicio = time.time()